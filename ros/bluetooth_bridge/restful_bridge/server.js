#!/usr/bin/node
const express = require('express');
const rosEmitter = require('./ros-subscriber');

var lidarData = {};
var posData = {};
rosEmitter.lidarMsg.on('update', (lidarDataUpdate) => {lidarData=lidarDataUpdate;})
rosEmitter.posMsg.on('update', (posDataUpdate) => {posData=posDataUpdate;})

const app = express();

app.get('/scan', function (req, res) {
  res.end(JSON.stringify(lidarData));
})

app.get('/pos', function (req, res) {
  res.end(JSON.stringify(posData))
})

var server = app.listen(8090, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})
